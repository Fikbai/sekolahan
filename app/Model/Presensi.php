<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Presensi extends Model
{
    protected $table = 'presensi';
    protected $guarded = ['id'];

    public function siswas(){
        return $this->belongsTo('App/Model/Siswa','siswa_id');
    }
}
